<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Cookie as HttpFoundationCookie;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function postRegister(Request $request)
    {
        $model = User::create([
            'fname' => $request->input('fname'),
            'mname' => $request->input('mname'),
            'lname' => $request->input('lname'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);

        if(!$model->save()) {
            return response([
                'message' => 'Faild!'
            ], status: Response::HTTP_BAD_REQUEST);
        }
        return response(['message' => 'Success!'], status: Response::HTTP_OK);
    }

    public function postLogin(Request $request)
    {
        if(!Auth::attempt($request->only('email', 'password'))) {
            return response([
                'message' => 'Wrong credentials!'
            ], status: Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();

        $token = $user->createToken('token')->plainTextToken;

        $cookie = cookie('jwt', $token, 60 * 24);

        return response([
            'message' => 'Success!'
        ], status: Response::HTTP_OK)->withCookie($cookie);
    }

    public function getUser()
    {
        return Auth::user();
    }

    public function postLogout()
    {
        $cookie = Cookie::forget('jwt');

        return response([
            'message' => 'Success!'
        ], status: Response::HTTP_OK)->withCookie($cookie);
    }
}
